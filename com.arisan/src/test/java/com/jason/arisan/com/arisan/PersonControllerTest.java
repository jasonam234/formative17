package com.jason.arisan.com.arisan;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PersonControllerTest {
	private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private static final PrintStream originalOut = System.out;
	private static final PrintStream originalErr = System.err;
	
	@BeforeEach
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@AfterEach
	public void restoreStreams() {
	    System.setOut(originalOut);
	    System.setErr(originalErr);
	}
	
	@BeforeEach
	@DisplayName("Setting data for all test")
	void setData() {
		PersonController.personList.add(new PersonModel("Person a", "aaaAAAaaa"));
		PersonController.personList.add(new PersonModel("Person b", "bbbBBBbbb"));
		PersonController.personList.add(new PersonModel("Person c", "cccCCCccc"));
		PersonController.ticketPool.add("aaaAAAaaa");
		PersonController.ticketPool.add("bbbBBBbbb");
		PersonController.ticketPool.add("cccCCCccc");
	}
	
	@Test
	void truePersonWinner() {
		String winner;
		int randomNumber = ThreadLocalRandom.current().nextInt(0, PersonController.ticketPool.size());
		winner = PersonController.setWinnerPerson(randomNumber);
		
		assertEquals(PersonController.personList.get(randomNumber).getName(), winner);
	}
	
	@Test
	void trueTicketWinner() {
		String winner;
		int randomNumber = ThreadLocalRandom.current().nextInt(0, PersonController.ticketPool.size());
		winner = PersonController.setWinnerTicket(randomNumber);
		
		assertEquals(PersonController.ticketPool.get(randomNumber), winner);
	}
	
	@Test
	void outOfBoundPersonWinner() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			PersonController.setWinnerPerson(10);
		});
	}
	
	@Test
	void outOfBoundTicket() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			PersonController.setWinnerTicket(100);
		});
	}
	
	@Test 
	void winnerRemoveTest(){
		PersonController.getWinner();
		assertTrue(PersonController.personList.size() == 0);
	}
	
	@Test 
	void ticketRemoveTest(){
		PersonController.getWinner();
		assertTrue(PersonController.ticketPool.size() == 0);
	}
	
	@Test
	void setPersonTest() {
		PersonController pc = new PersonController();
		
		String input = "6";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		assertEquals(6, pc.setPersonCount());
	}
	
	@Test
	void setPersonExceptionTest() {
		PersonController pc = new PersonController();
		
		String input = "aaa";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		
		assertThrows(InputMismatchException.class, () -> {
			pc.setPersonCount();
		});
	}
	
	@Test
	void setMoneyTest() {
		PersonController pc = new PersonController();
		
		String input = "1000";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		assertEquals(1000, pc.setMoneyCount());
	}
	
	@Test
	void setMoneyExceptionTest() {
		PersonController pc = new PersonController();
		
		String input = "aaa";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		
		assertThrows(InputMismatchException.class, () -> {
			pc.setMoneyCount();
		});
	}
	
	@Test
	void printHeader() {
		int result = PersonController.printHeader();
		assertEquals(1, result);
	}
	
	@Test 
	void printPersonList(){
		int result = PersonController.getPersonList();
		assertEquals(1, result);
	}
}
