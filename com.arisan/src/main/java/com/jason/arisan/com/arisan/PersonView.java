package com.jason.arisan.com.arisan;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class PersonView {
	public static void header() {
		System.out.println("=====================================");
		System.out.println("Arisan Randomizer");
		System.out.println("=====================================");
	}
	
	public void listOfPerson(ArrayList<PersonModel> person) {
		for(int i = 0; i < person.size(); i++) {
			System.out.println("Nama peserta      : " + person.get(i).getName());
			System.out.println("Nomor ticket      : " + person.get(i).getTicket());
			System.out.println("=====================================");
		}
	}
	
	public static void winner(int money, String person, String ticket) {
		Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        
		System.out.println("Ticket hari ini : " + ticket);
		System.out.println("Pemenang hari ini : " + person + ", mendapatkan " + formatRupiah.format(money));
		System.out.println("=====================================");
	}
}