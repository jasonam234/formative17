package com.jason.arisan.com.arisan;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomStringUtils;

public class PersonController {
	private static PersonModel model;
	private static PersonView view;
	
	static int totalMoney;
	
	static Scanner scanner = new Scanner(System.in);
	
	static ArrayList<PersonModel> personList = new ArrayList<PersonModel>();
	static ArrayList<String> ticketPool = new ArrayList<String>();
	
	public PersonController() {

	}
	
	public PersonController(PersonModel model, PersonView view) {
		this.model = model;
		this.view = view;
	}
	
	public static int printHeader() {
		view.header();
		return 1;
	}
	
	public static int setPersonCount() {
		Scanner scanner = new Scanner(System.in);
		int personCount = 0;
		try {
			System.out.print("Jumlah orang          : ");
			return scanner.nextInt();
		}catch(Exception e) {
			System.out.println("Invalid Input");
			throw e;
		}
	}
	
	public static int setMoneyCount() {
		Scanner scanner = new Scanner(System.in);
		int moneyCount = 0;
		try {
			System.out.print("Jumlah uang per orang : ");
			return scanner.nextInt();
		}catch(Exception e) {
			System.out.println("Invalid Input");
			throw e;
		}
	}
	
	public void setData() {
		String name;
		String ticket;
		
		int personCount = setPersonCount();
		int moneyCount = setMoneyCount();
		totalMoney = personCount * moneyCount;
		
		for(int i = 0; i < personCount; i++) {
			System.out.print("Masukkan nama orang ke-" + (i+1) + " : ");
			name = scanner.nextLine();
			name.strip();
			ticket = RandomStringUtils.randomAlphabetic(7);
			personList.add(new PersonModel(name, ticket));
			ticketPool.add(ticket);
		}
	}
	
	public static int getPersonList() {
		view.listOfPerson(personList);
		return 1;
	}
	
	public static void getWinner() {
		while(personList.size() > 0 && ticketPool.size() > 0) {
			int randomNumber = ThreadLocalRandom.current().nextInt(0, ticketPool.size());
			view.winner(totalMoney, setWinnerPerson(randomNumber), setWinnerTicket(randomNumber));
			ticketPool.remove(randomNumber);
			personList.remove(randomNumber);
		}
	}
	
	public static String setWinnerPerson(int randomNumber) {
		return personList.get(randomNumber).getName();
	}
	
	public static String setWinnerTicket(int randomNumber) {
		return ticketPool.get(randomNumber);
	}
}
