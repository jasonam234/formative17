package com.jason.arisan.com.arisan;

public class PersonModel {
	private String name;
	private String ticket;
	
	public PersonModel() {
	}
	
	public PersonModel(String newName, String newTicket) {
		this.name = newName;
		this.ticket = newTicket;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getTicket() {
		return this.ticket;
	}
}
