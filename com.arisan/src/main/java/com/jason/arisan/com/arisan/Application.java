package com.jason.arisan.com.arisan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Arisan Randomizer
 * @author NEXSOFT
 * @version 1.0.1
 * @since 21/10/2021
 */

@SpringBootApplication
public class Application {

	public static int main(String[] args) {
		PersonView view = new PersonView();
		PersonModel model = new PersonModel();
		PersonController controller = new PersonController(model, view);
		
		controller.printHeader();
		try {
			controller.setData();
		}catch(Exception e) {
			e.getMessage();
		}
		controller.getPersonList();
		controller.getWinner();
		
		return 0;
	}
}